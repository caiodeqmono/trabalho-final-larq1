# README #

Essa é uma implementação da arquitetura MIPS para a disciplina Laboratório de arquitetura de computadores 1 da Universidade federal de são carlos.
Essa implementação visa a modificação da arquitetura MIPS multiciclo para que a memória de instruções e dados seja armazenada no mesmo componente.
O objetivo é ver implementado na máquina MIPS o modelo de Von Neumann.

### Como está implementado ###

* O datapath foi alterado para remover o módulo de memória de instruções.
* As moduficações do datapath adicionam capacidade de leitura e escrita em um módulo único de memória. O programa está armazenado no mesmo componente que os dados.
* O programa pode modificar suas instruçoes como se fossem dados [ref1](http://en.wikipedia.org/wiki/Von_Neumann_architecture) [ref2](http://en.wikipedia.org/wiki/Self-modifying_code) .
* Foi implementada uma instrução JRI (Jump Remove Instruction) para mostrar que é possível que sejam feitas alterações na memória de instruções pelo próprio código.

### Como fazer o set up para trabalhar nesse repositório ###

* Baixar o [model sim](http://www.altera.com/products/software/quartus-ii/modelsim/qts-modelsim-index.html)
* O [notepad++](http://notepad-plus-plus.org/) [npp-portable](http://portableapps.com/apps/development/notepadpp_portable) como IDE e o model sim para fazer os testes
* O [MARS](http://courses.missouristate.edu/KenVollmar/MARS/) para simular alguns testes também

### Regras de contribuição ###

* Todos os commits devem documentar todas as changes
* Não fazer commits para changes pequenas demais
* Tente acumular pelomenos dois commits para dar um push. Só de pushes significativos
* Cada desenvolvedor deve ter um branch com seu nome e dev: lucas-dev, caio-dev, jun-dev
* Quando pronta uma série de mudanças fazer uma pull request aqui no site do seu branch para o master

### Com quem falar? ###

* Fale com o Lucas para resolver pull requests
* Issues Fale com Caio

### Material de referencia ###
* [Artigo de Von Neumann e MIPS](http://www.math-cs.gordon.edu/courses/cs311/lectures-2011/VonNeumannArchIntroMIPS.pdf)
* [Harvard versus Von Neumann](http://eecs.wsu.edu/~aofallon/ee234/lectures/ComputerArchitectureOverview.pdf)
* [Slide de arquitetura Von Neumann e harvard](http://www.inf.pucrs.br/~emoreno/undergraduate/CC/orgarqi/class_files/Aula17/Aula17b.pdf)

### Esquemático base ###
![mprc.pku.edu.cn_courses_organization_autumn2012_COD-1.png](https://bitbucket.org/repo/xG4GMb/images/966129394-mprc.pku.edu.cn_courses_organization_autumn2012_COD-1.png)