library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity processor is
	port (clock, turn_off: in std_logic;
		instruction_address, current_instruction, data_in_last_modified_register, video_out: 
		out std_logic_vector (31 downto 0);
    video_address: in std_logic_vector(11 downto 0));
end processor;

architecture behavioral of processor is

	component program_counter 
		generic (address_width: integer := 32);
	port (
		clock, enable, jump: in std_logic;
		next_address: out std_logic_vector (address_width - 1 downto 0);
		jump_address: in std_logic_vector (address_width - 1 downto 0));
	end component;

	component state_register 
		generic (width: integer := 32);
		port (
			clock: in std_logic;
			input: in std_logic_vector (width - 1 downto 0);
			output: out std_logic_vector (width - 1 downto 0));
	end component;

	component memory 
		generic (
		address_width: integer := 12;
		data_width: integer := 32);

		port (
		clock: std_logic;
		address, video_address: in std_logic_vector (address_width - 1 downto 0);
		data_to_write: in std_logic_vector (data_width - 1 downto 0);
		read, write: in std_logic;
		data_out, video_out: out std_logic_vector (data_width - 1 downto 0));
	end component;

	component control_unit
	port (
		clock: in std_logic;
		instruction: in std_logic_vector (31 downto 0);
		enable_program_counter: out std_logic := '0';
		register1, register2, register3: out std_logic_vector (4 downto 0);
		immediate: out std_logic_vector (15 downto 0);
		write_register, mem_to_register: out std_logic;
		reg_dst: out std_logic;
		source_alu: out std_logic_vector (1 downto 0);
		alu_operation: out std_logic_vector (2 downto 0);
		read_memory, write_memory: out std_logic;
		offset: out std_logic_vector (31 downto 0);
		jump_control: out std_logic;
		jump_offset: out std_logic_vector(25 downto 0);
		i_or_d: out std_logic;
		write_noop: out std_logic
		);
	end component;

	component register_bank
		generic (width: integer := 32);

		port (
			clock: in std_logic;
			register_to_read1, register_to_read2, register_to_write: 
			in std_logic_vector (4 downto 0);
		write: in std_logic;
		data_to_write: in std_logic_vector (width - 1 downto 0);
		data_out1, data_out2: out std_logic_vector (width - 1 downto 0));
	end component;

	component alu_x
		generic (width: integer := 32);

		port (
			a, b: in std_logic_vector (width - 1 downto 0);
		operation: in std_logic_vector (2 downto 0);
		result: out std_logic_vector (width - 1 downto 0));
	end component;

	component extend
		generic (width: integer := 32);

		port (
			input: in std_logic_vector (15 downto 0);
			output: out std_logic_vector (width - 1 downto 0)
		);
	end component;
	
	signal clk: std_logic;

	-- control signals for state elements.
	signal enable_program_counter, jump_control: std_logic;

	-- Signals related to the instruction fetch state.
	signal address_of_next_instruction, instruction, data_from_instruction_register, jump_address: 
			std_logic_vector (31 downto 0);
	signal	jump_offset: std_logic_vector(25 downto 0);

	-- Signals related to the banck of registers.
	signal destination_register, register1, register2, register3: std_logic_vector (4 downto 0);
	signal data_to_write_in_register: std_logic_vector (31 downto 0); 
	signal write_register, mem_to_register: std_logic;

	-- Signals related to the ALU.
	signal alu_operand1, alu_operand2, alu_op2_mux: std_logic_vector(31 downto 0);
	signal register_a, register_b, alu_result, 		
	  data_from_alu_output_register: std_logic_vector (31 downto 0);
	signal immediate_data: std_logic_vector (15 downto 0);
	signal ext_immediate_data: std_logic_vector (31 downto 0);
	signal reg_dst: std_logic;
	signal source_alu: std_logic_vector (1 downto 0); 
	signal alu_operation: std_logic_vector (2 downto 0); 

	-- Signals related to the memory access.
	signal address, middle_address: std_logic_vector (31 downto 0);
	signal data_from_memory, data_from_memory_register, offset: std_logic_vector (31 downto 0);
	signal read_memory, write_memory: std_logic;

	-- Von neumann memory control signal
	signal i_or_d: std_logic;

	--Self modifying instruction
	signal write_memory_noop_mux: std_logic_vector (31 downto 0);
	signal write_noop: std_logic;
	
begin

    instruction_address <= address_of_next_instruction;
		--alu_operand1 <= register_a; already connected
		
		alu_op2_mux <= alu_operand2 when source_alu = "00"
			else offset when source_alu = "01"
			else ext_immediate_data when source_alu = "10"
			else offset;
		data_to_write_in_register <= data_from_memory_register when mem_to_register = '1' else data_from_alu_output_register; 
		destination_register <= register2 when reg_dst = '0' else register3;
		
		middle_address <= data_from_alu_output_register + 64 when i_or_d = '1' else address_of_next_instruction; -- +64 is the data base address
		address <= middle_address-1 when write_noop = '1' else middle_address;
		-- address_to_write <= data_from_alu_output_register;

		--Connect to instruction
		instruction <= data_from_memory;
		
		current_instruction <= instruction;
		data_in_last_modified_register <= data_to_write_in_register;
		
		jump_address <= address_of_next_instruction(31 downto 26) & jump_offset;

		pc: program_counter port map (
		  clk, 
		  enable_program_counter, 
		  jump_control, 
		  address_of_next_instruction, 
		  jump_address);

		instruction_register: state_register port map (clk, instruction, data_from_instruction_register); 

		state_machine: control_unit port map (
		  clk, 
		  instruction, 			
		  enable_program_counter,  			
		  register1, 
		  register2, 
		  register3,
		  immediate_data,
      write_register,
		  mem_to_register, 
		  reg_dst,
		  source_alu, 
		  alu_operation, 			
		  read_memory, 
		  write_memory, 
		  offset, 
		  jump_control, 
		  jump_offset,
		  i_or_d,
		  write_noop); 

		bank_of_registers: register_bank port map (
		  clk, 
		  register1, 
		  register2, 
		  destination_register, 		
		  write_register,
		  data_to_write_in_register, 
		  register_a, 
		  register_b);  

		 sign_extender: extend port map(
			immediate_data,
			ext_immediate_data
		 );
		  
		alu_input_register_a: state_register port map (clk, 	register_a, alu_operand1);

		alu_input_register_b: state_register port map (clk, 	register_b, alu_operand2);

		alu: alu_x port map (alu_operand1, alu_op2_mux, alu_operation, alu_result);

		alu_output_register: state_register port map (clk, alu_result, data_from_alu_output_register);

		write_memory_noop_mux <= alu_operand2 when write_noop = '0'
			else "00000000000000000000000000000000";
		
		memory_mem : memory port map (
		  clock,  --clock
		  address(11 downto 0),  --addr
          video_address,
		  write_memory_noop_mux,  --Data to write
		  read_memory,  --read 
		  write_memory,  --write
		  data_from_memory);    -- dataout

		memory_data_register: state_register port map (clk, 	data_from_memory, data_from_memory_register);

		process (clock, turn_off)
		begin
			if turn_off /= '1' then
				clk <= clock;
      else
        clk <= '0';
			end if; 
		end process;
 
end behavioral;

