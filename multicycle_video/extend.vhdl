library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity extend is
	generic (width: integer := 32);
	port (
	input: in std_logic_vector (15 downto 0);
	output: out std_logic_vector (width - 1 downto 0)
	);
end;

architecture structural of extend is
begin
	process (input)
	begin
		output <= (others => input(15));
		output(15 downto 0) <= input;
	end process;
end structural;

