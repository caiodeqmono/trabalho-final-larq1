library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity memory is
	generic (
		address_width: integer := 12;
		data_width: integer := 32);

	port (
		clock: std_logic;
		address, video_address: in std_logic_vector (address_width - 1 downto 0);
		data_to_write: in std_logic_vector (data_width - 1 downto 0);
		read, write: in std_logic;
		data_out, video_out: out std_logic_vector (data_width - 1 downto 0));
end;

architecture behavioral of memory is

	type data_sequence is array (0 to 2**address_width - 1) of std_logic_vector (data_width - 1 downto 0);  
	
	--Teste JRI
	--signal data: data_sequence := 
		-- addi $t0, $0, 1
    --(	--0 => X"001000 00000 01000 0000000000000001",
	--	0 => "00100000000010000000000000000001",
		-- addi $t1, $0, 3
		--1 => X"001000 00000 01001 0000000000000011",
	--	1 => "00100000000010010000000000000011",
		-- add $t0, $t0, $t1
		-- 000000 01000 01001 01000 00000100000
	--	2 => X"01094020",
		-- JRI 000010 ii iiii iiii iiii iiii iiii iiii
	--	3 => "10000000000000000000000000000001",
		--TEST
	--	4 => "00100000000010000000000000000001",

	--	others => (others => 'U'));
		
		
	--Teste JMP normal
	--signal data: data_sequence := 
		-- addi $t0, $0, 1
    --(	--0 => X"001000 00000 01000 0000000000000001",
	--0 => "00100000000010000000000000000001",
		-- addi $t1, $0, 3
		--1 => X"001000 00000 01001 0000000000000011",
	--1 => "00100000000010010000000000000011",
		-- add $t0, $t0, $t1
		-- 000000 01000 01001 01000 00000100000
	--2 => X"01094020",
		-- JMP 0000 10 00000000000000000000000001
	--3 => "00001000000000000000000000000001",
		--TEST
	--4 => "00100000000010000000000000000001",

	--others => (others => 'U'));
		
		
	--JRI soma ate 5
	
	--signal data: data_sequence := 
		-- addi $t0, $0, 0
	--(	--0 => "001000 00000 01000 0000000000000000",
		--0 => "00100000000010000000000000000000",
		-- addi $t0, $t0, 1
		
		--1 => addi "001000 01000 01001 0000000000000001",
		--1 => "00100001000010000000000000000001",
		
		--5 JRI calls
		-- JRI 000010 00000000000000000000000001
		--2 => "10000000000000000000000000000001",
		-- JRI 000010 00000000000000000000000001
		--3 => "10000000000000000000000000000001",
		-- JRI 000010 00000000000000000000000001
		--4 => "10000000000000000000000000000001",
		-- JRI 000010 00000000000000000000000001
		--5 => "10000000000000000000000000000001",
		-- JRI 000010 00000000000000000000000001
		--6 => "10000000000000000000000000000001",
		
		--Store t0 in t1 to see result
		--add $t1, $t0, $0
		-- 000000 01000 00000 01001 00000100000
		--7 => "00000001000000000100100000100000",
		
		--others => (others => 'U'));
		
	
	--Test memory with data base 64
	
	signal data: data_sequence := 
		(
		-- addi $t0, $0, 4 "001000 00000 01000 0000000000000100",
		0 => "00100000000010000000000000000100",
		
		-- sw $t0, 0($0) "101011 00000 01000 0000000000000000",
		1 => "10101100000010000000000000000000",
		
		-- lw $t1, 0($0) "100011 00000 01001 0000000000000000",
		2 => "10001100000010010000000000000000",
		
		--3 => addi $t1, $0, 0 "001000 00000 01001 0000000000000000" 
		3 => "00100000000010010000000000000000",
		
		others => (others => 'U'));
		
-- Quartus II
--	signal data: data_sequence;

begin

 
  
  
   process(clock)
		variable index: integer := 0;
   begin
   if(rising_edge(clock)) then -- Port A
        -- Only fetch a new index on read
		if(read = '1') then
    			index := to_integer(unsigned(address));
		end if;
		
		-- Write also fetches a index
       if(write = '1') then
				index := to_integer(unsigned(address));
   	    		data(index) <= data_to_write;
          -- Read-during-write on the same port returns NEW data
          data_out <= data_to_write;
       else
          -- Read-during-write on the mixed port returns OLD data
          data_out <= data(index);
       end if;
   end if;
   end process;

    read_video: process (clock)
		variable index: integer;
	begin
    if(rising_edge(clock)) then -- Port B
			index := to_integer(unsigned(video_address));
			video_out <= data(index);
    end if;
	end process;

end behavioral;

